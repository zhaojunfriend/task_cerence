package com.pio;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat;

/**
 * @author zhaojun
 */
public class MultipleTextOutputFormatByKey<K, V> extends MultipleTextOutputFormat<K, V> {

	@Override
	protected String generateFileNameForKeyValue(K key, V value, String leaf) {
		return new Path(key.toString(), leaf).toString();
	}

	@Override
	protected K generateActualKey(K key, V value) {
		return null;
	}
}
