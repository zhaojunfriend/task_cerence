package com.pio;

import org.apache.flink.api.common.functions.*;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.hadoop.mapred.HadoopOutputFormat;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;

/**
 * @author zhaojun
 */
public class Application {

	/**
	 * 整行日志以空格分开 静态资源路径在整个日志文件的位置
	 */
	private static final int URL_STATICS_RESOURCE_MIN_LENGTH = 6;

	/**
	 * 静态资源路径在整个日志文件的位置
	 */
	private static final int URL_STATICS_RESOURCE_POS = 6;

	/**
	 * 以/static开头的资源文件,以 "/" 切割之后最小长度
	 */
	private static final int STATICS_RESOURCE_NAME_SPLIT_MIN_LENGTH = 2;

	private static final String STATIC_HEAD = "/static";

	private static final String QUESTION_MARK = "?";

	/**
	 * 日志 hdfs 路径
	 */
	private static final String LOG_FILE_PATH = "hdfs://namenode:9000/flink/input/access_log";

	/**
	 * 输出文件路径
	 */
	private static final String OUT_FILE_PATH = "hdfs://namenode:9000/flink/";

	/**
	 * 入口函数
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		/**
		 * 获取运行环境
		 */
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		/**
		 * 从 hdfs 读取数据
		 */
		DataSet<String> text = env.readTextFile(LOG_FILE_PATH);

		/**
		 * 初始化 hadoop jobConf
		 */
		JobConf jc = new JobConf();
		/**
		 * 设置输出路径
		 */
		FileOutputFormat.setOutputPath(jc, new Path(OUT_FILE_PATH));

		/**
		 * 多文件输出
		 */
		MultipleTextOutputFormatByKey format = new MultipleTextOutputFormatByKey();
		HadoopOutputFormat<String, String> hadoopOutputFormat = new HadoopOutputFormat<String, String>(format, jc);

		text
				.map(new MapFunction<String, String>() {
					@Override
					public String map(String s) {
						if (!s.isEmpty()) {
							String[] splits = s.split(" ");
							if (splits.length > URL_STATICS_RESOURCE_MIN_LENGTH) {
								/**
								 * 获取静态资源信息
								 */
								return splits[URL_STATICS_RESOURCE_POS];
							}
						}
						return null;
					}
				})
				/**
				 * 过滤不是以 /static 开始的数据
				 */
				.filter(new FilterFunction<String>() {
					@Override
					public boolean filter(String s) {
						return (null != s) && (s.trim().startsWith(STATIC_HEAD));
					}
				})
				/**
				 * 对数据以 "/" 切割, 得到静态资源文件类型和静态资源文件名
				 */
				.map(new MapFunction<String, Tuple2<String, String>>() {
					@Override
					public Tuple2<String, String> map(String s) {
						String[] split = s.split("/");
						if (split.length > STATICS_RESOURCE_NAME_SPLIT_MIN_LENGTH) {
							/**
							 * 获取静态资源名称及类型
							 */
							String fileType = split[split.length - 2];
							String fileName = split[split.length - 1];
							if (fileName.contains(QUESTION_MARK)) {
								String[] filenameArr = fileName.split("\\?");
								return Tuple2.of(fileType, filenameArr[0]);
							} else {
								return Tuple2.of(fileType, fileName);
							}
						} else {
							return null;
						}
					}
				})
				/**
				 * 过滤为空的数据, 上一步操作可能返回 null
				 */
				.filter(new FilterFunction<Tuple2<String, String>>() {
					@Override
					public boolean filter(Tuple2<String, String> value) {
						return (null != value);
					}
				})
				/**
				 * 以文件名去重, 索引为 1
				 */
				.distinct(1)
				.output(hadoopOutputFormat);

		env.execute("Process Log File");
	}
}
