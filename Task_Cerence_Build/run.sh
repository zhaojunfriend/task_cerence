#!/bin/bash

if [ "$1" = "jobmanager" ]; then
    echo "Starting Job Manager"
    ls -l /opt/
    ls -l $FLINK_HOME
    ls -l $FLINK_HOME/conf/
    #sed -i -e "s/jobmanager.rpc.address: localhost/jobmanager.rpc.address: namenode/g" $FLINK_HOME/conf/flink-conf.yml

    namedir=`echo $HDFS_CONF_dfs_namenode_name_dir | perl -pe 's#file://##'`
    if [ ! -d $namedir ]; then
      echo "Namenode name directory not found: $namedir"
      exit 2
    fi

    if [ -z "$CLUSTER_NAME" ]; then
      echo "Cluster name not specified"
      exit 2
    fi

    if [ "`ls -A $namedir`" == "" ]; then
      echo "Formatting namenode name directory: $namedir"
      $HADOOP_HOME/bin/hdfs --config $HADOOP_CONF_DIR namenode -format $CLUSTER_NAME
    fi

    echo "config file: " && grep '^[^\n#]' $FLINK_HOME/conf/flink-conf.yml
    $FLINK_HOME/bin/jobmanager.sh start cluster
    $HADOOP_HOME/bin/hdfs --config $HADOOP_CONF_DIR --daemon start namenode
    $HADOOP_HOME/bin/hdfs --config $HADOOP_CONF_DIR secondarynamenode

elif [ "$1" = "taskmanager" ]; then
    ls -l /opt/
    ls -l $FLINK_HOME
    ls -l $FLINK_HOME/conf/
    #sed -i -e "s/jobmanager.rpc.address: localhost/jobmanager.rpc.address: namenode/g" $FLINK_HOME/conf/flink-conf.yml
    sed -i -e "s/taskmanager.numberOfTaskSlots: 1/taskmanager.numberOfTaskSlots: `grep -c ^processor /proc/cpuinfo`/g" $FLINK_HOME/conf/flink-conf.yml

    echo "Starting Task Manager"
    echo "config file: " && grep '^[^\n#]' $FLINK_HOME/conf/flink-conf.yml
    $FLINK_HOME/bin/taskmanager.sh start
    $HADOOP_HOME/bin/hdfs --config $HADOOP_CONF_DIR datanode
    
else
    $@
fi