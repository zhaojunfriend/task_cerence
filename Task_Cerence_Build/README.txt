1. 下载 jdk-8u151-linux-x64.tar.gz
2. 下载 hadoop-3.1.2.tar.gz
3. 下载 flink-1.8.2-bin-scala_2.11.tgz
4. 把3个下载好的文件，拷贝至 Dockerfile 所在的目录
5. 执行docker build -t flink .
6. 第5步成功之后，执行 docker-compose -f docker-compose-flink.yml up -d